---
title: Apple II arduno based dma board with wifi and bluetooth
author: Willow Behar
geometry: margin=.5in
---

# Features

- DMA a file into a specified area of memory

# Communication Protocol

## Protocol Types

- Binary File Load = $00
- Binary File Save = $01
- Wifi Connect = $02
- Wifi Disconnect = $03
- Wifif List = $04
- Wifi Reconnect = $05
- Bluetooth Connect = $06
- Bluetooth Disconnect = $07
- Bluetooth List = $08

## Binary File Load

| Byte Number | Use |
| :---: | :---: |
| 0 | Communication Protocol |
| 1 | Starting Hi Byte |
| 2 | Starting Low Byte | 
| 3 | Length Hi Byte |
| 4 | Length Low Byte |
| 5... | File Byte |

## Binary File Save

| Byte Number | Use |
| :---: | :---: |
| 0 | Communication Protocol |
| 1 | Starting Hi Byte |
| 2 | Starting Lo Byte |
| 3 | Length Hi Byte |
| 4 | Length Lo Byte |

## Wifi Connect

| Byte Number | Use |
| :---: | :---: |
| 0 | Connection Protocol |
| 1-"\n" | Wifi Name (SSID) |
| "\n" - "\n" | Wifi Password |

## Wifif Disconnect

| Byte Number | Use |
| :---: | :---: |
| 0 | Connection Protocol |

## Wifi List

| Byte Number | Use |
| :---: | :---: |
| 0 | Connection Protocol |

## Wifi Reconnect

| Byte Number | Use|
| :---: | :---: |
| 0 | Connection Protocol |


# Atmega Connection Table

| Atmega 428p | Connection |
| :---: | :---: | 
| Pin 1 - Reset | 
| Pin 2 - RX | USB | 
| Pin 3 - TX | USB | 
| Pin 4 - DP | Soft Serial RX |
| Pin 5 - DP | Soft Serial TX |
| Pin 6 - DP | ESP Flash |
| Pin 7 - VCC| |
| Pin 8 - GND | |
| Pin 9 - XTAL 1 | |
| Pin 10 - XTAL 2 | |
| Pin 11 - DP | I/O Data Clock |
| Pin 12 - DP | I/O Data Pin |
| Pin 13 - DP | I/O Data Latch |
| Pin 14 - DP | DMA Write Pin|
| Pin 15 - DP | ESP Power on/off - High: on, Low: off|
| Pin 16 - DP | |
| Pin 17 - DP | |
| Pin 18 - DP | |
| Pin 19 - DP | Data High /Data Low / Addr Select - p2|
| Pin 20 - Pos V | |
| Pin 21 - Ref V | |
| Pin 22 - GND | |
| Pin 23 - AI | Device Select - Low Active |
| Pin 24 - AI | Clock 0 |
| Pin 25 - AI | R/W High:Reading+add, Low:Writing Data+add |
| Pin 26 - AI | DMA Out |
| Pin 27 - AI | INT OUT |
| Pin 28 - AI | |

## Data Writting

## Software Overview

```


if(Dev Select == High && File_to_AppleII == True){
    DMA_Write()
}
if(Dev Select == Low && R/W == LOw){
    Read Data
}
if(Dev Select == Low $$ R/W == High){
    Write Data
}
```


# Parts

- Atmega 438p
- serial to usb
- USB c port
- 3x Shift Registers
- ESP8266
- BlueSMIrf


# Wifi Software Protocol

```python
wlan = network.WLAN()
uuid = ""
password = ""
server_port = 4545
s_connections = {}

def connect(UUID, Password):
    uuid = UUID
    password = Password
    wlan.connect(UUID, Password)

def reconnect():
    wlan.disconnect()
    wlan.connect(uuid, password)

def sockConnect(IP, Port, Type):
    s_connections[IP] = socket.so

def 

```