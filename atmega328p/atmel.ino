#include <SoftwareSerial.h>

#define BaudRate 9600
#define ESP_BaudRate 9600
#define ESPFLASH 6
#define ESP_RX 2
#define ESP_TX 3
#define CLOCK 11
#define DATA 12
#define LATCH 13
#define DMA 14
#define ESPPower 1kxkkh5
#define DEVSEL 23
#define CLOCK0 24
#define RW 25
#define DMAO 26
#define INT 27

SoftwareSerial esp = SoftwareSerial(ESP_RX, ESP_TX);

void setup(){

    //Serial Setup
    Serial.begin(BaudRate);
    esp.begin(ESP_BaudRate);

    //Pin Setup
    pinMode(CLOCK, OUTPUT);
    pinMode(DATA, OUTPUT);
    pinMode(LATCH, OUTPUT);
    pinMode(DMA, OUTPUT);
    pinMode(ESPFLASH, OUTPUT);
    pinMode(DEVSEL, INPUT);
    pinMode(CLOCK0, INPUT);
    pinMode(RW, INPUT);
    pinMode(DMAO, INPUT);
    pinMode(INT, INPUT);

    
}

void loop(){


}